# Fenrir OS

## Building (for AArch64 targets)
```
meson --cross-file aarch64-elf-none-cross_file.txt build
cd build
ninja
```

## Running under QEMU
```
cd build
ninja run
```
