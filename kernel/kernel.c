#include <libfdt.h>

#include "uart.h"

void
kmain (void *fdt)
{
    if (fdt_check_header (fdt) != 0)
    {
        return;
    }

    uart_init (fdt);
    uart_printf ("Hello, world!");
}
