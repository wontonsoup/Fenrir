#pragma once

#include <stdbool.h>

bool uart_init (const void *);
void uart_printf (const char *, ...);
