#include "uart.h"

#include <stdarg.h>
#include <stddef.h>

#include <libfdt.h>

static volatile char *uart = NULL;

bool
uart_init (const void *fdt)
{
    int node_chosen;
    const void *prop_stdout_path;
    int node_stdout_path;
    const void *prop_status;
    const fdt32_t *prop_address_cells;
    uint32_t num_address_cells;
    const fdt32_t *prop_reg;
    uintptr_t uart_ptr;

    node_chosen = fdt_path_offset (fdt, "/chosen");
    if (node_chosen < 0)
    {
        return false;
    }
    prop_stdout_path = fdt_getprop (fdt, node_chosen, "stdout-path", NULL);
    if (prop_stdout_path == NULL)
    {
        return false;
    }
    node_stdout_path = fdt_path_offset (fdt, prop_stdout_path);
    if (node_stdout_path < 0)
    {
        return false;
    }
    prop_status = fdt_getprop (fdt, node_stdout_path, "status", NULL);
    if (strcmp (prop_status, "okay") != 0)
    {
        return false;
    }
    prop_address_cells =
        fdt_getprop (fdt, node_stdout_path, "#address-cells", NULL);
    if (prop_address_cells == NULL)
    {
        num_address_cells = 2;
    }
    else
    {
        num_address_cells = fdt32_to_cpu (*prop_address_cells);
    }
    prop_reg = fdt_getprop (fdt, node_stdout_path, "reg", NULL);
    if (prop_reg == NULL)
    {
        return false;
    }

    /* Assuming there aren’t any surprises wrt the cell count. */
    for (uint32_t i = 0; i < num_address_cells; i++)
    {
        uart_ptr <<= 32;
        uart_ptr |= fdt32_to_cpu (*(prop_reg++));
    }

    uart = (volatile char *)uart_ptr;

    return true;
}

void
uart_printf (const char *format,
             ...)
{
    __builtin_va_list ap;

    __builtin_va_start (ap, format);

    while (*format != 0)
    {
        if (*format != '%')
        {
            *uart = *format;
        }

        (void)__builtin_va_arg (ap, int);

        format++;
    }

    __builtin_va_end (ap);
}
