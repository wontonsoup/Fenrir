.text
.globl _start

_start:
    adrp x1, stack_top
    mov sp, x1

    mrs x1, cpacr_el1
    // Do not trap accesses to SIMD and floating-point registers.
    // FPEN <- 0b11
    orr x1, x1, 0x300000
    msr cpacr_el1, x1

    b kmain

.bss
.align 16
stack_bottom = .
. = . + 0x4000
stack_top = .
